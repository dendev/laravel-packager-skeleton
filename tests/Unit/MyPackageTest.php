<?php

namespace Tests\Unit;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Carbon;
use Orchestra\Testbench\TestCase;

class :uc:packageTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $loader = AliasLoader::getInstance();
        $loader->alias(':uc:package', '\:uc:vendor\:uc:package\Facades\:uc:packageFacade');
    }

    protected function getPackageProviders($app)
    {
        return [
            ':uc:vendor\:uc:package\:uc:packageServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        /*
        $config = include './tests/config.php';

        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
        */
    }


   public function testBasic()
    {
        $this->assertEquals(':lc:package', \:uc:package::test_me());
    }}
