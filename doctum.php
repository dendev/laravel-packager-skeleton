<?php

use Doctum\Doctum;
use Symfony\Component\Finder\Finder;
use Doctum\RemoteRepository\GitHubRemoteRepository;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('resources')
    ->exclude('tests')
    ->in('./src');


return new Doctum($iterator, [
    'title'                => ':uc:package',
    'language'             => 'fr', // Could be 'fr'
    'build_dir'            => __DIR__ . '/docs/build',
    'cache_dir'            => __DIR__ . '/docs/cache',
    'remote_repository'    => new GitHubRemoteRepository(':uc:vendor\:uc:package', 'https://gitlab.com/:lc:vendor/:lc:package'),
    'default_opened_level' => 2,
]);
